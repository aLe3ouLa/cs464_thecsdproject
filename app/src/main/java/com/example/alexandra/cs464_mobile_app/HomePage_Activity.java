package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;


public class HomePage_Activity extends ActionBarActivity {

    ActionBar actionBar = getSupportActionBar();

    private ImageButton mTeamListView;
    private ImageButton mMyProjects;
    private ImageButton mNewMeeting;
    private ImageButton mMyMeetings;

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page_);

        /* Initialize the fields*/
        mTeamListView = (ImageButton)findViewById(R.id.myTeams);
        mMyProjects = (ImageButton)findViewById(R.id.myProjects);
        mNewMeeting = (ImageButton)findViewById(R.id.newMeeting);
        mMyMeetings = (ImageButton)findViewById(R.id.myMeeting);

      // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);


        mTeamListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  Toast.makeText(HomePage_Activity.this, "My Groups", Toast.LENGTH_LONG).show();

                //take user to My Teams
                Intent takeUserTeams = new Intent(HomePage_Activity.this, GroupList.class);
                startActivity(takeUserTeams);

            }
        });

        mMyMeetings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(HomePage_Activity.this, "My Meetings", Toast.LENGTH_LONG).show();

                //take user to My Meetings
                Intent takeUserTeams = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.MeetingList.class);
                startActivity(takeUserTeams);

            }
        });

        mNewMeeting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // Toast.makeText(HomePage_Activity.this, "New Meeting", Toast.LENGTH_LONG).show();

                //take user to New meeting
                Intent takeUserTeams = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.CreateNewMeeting.class);
                startActivity(takeUserTeams);

            }
        });

        mMyProjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(HomePage_Activity.this, "My Projects", Toast.LENGTH_LONG).show();

                //take user to My Projects
                Intent takeUserTeams = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.ProjectList.class);
                startActivity(takeUserTeams);

            }
        });

       // Action Bar Buttons

        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(HomePage_Activity.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(HomePage_Activity.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(HomePage_Activity.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(HomePage_Activity.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(HomePage_Activity.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home_page_, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(HomePage_Activity.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
