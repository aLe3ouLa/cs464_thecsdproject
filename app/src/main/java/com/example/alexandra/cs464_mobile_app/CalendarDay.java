package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CalendarDay extends ActionBarActivity {

    private List<WeekList> myDay = new ArrayList<>();

    private Button bweek,bmonth,bday;

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_day);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarDay.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarDay.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarDay.this, "Search", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarDay.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(CalendarDay.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        //Buttons
        bmonth = (Button)findViewById(R.id.month);
        bweek = (Button)findViewById(R.id.week);
        bday = (Button)findViewById(R.id.days);

        // Calendar Buttons
        bmonth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarDay.this, "Month", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserMonth = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.CalendarView.class);
                startActivity(takeUserMonth);
            }
        });

        bweek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarDay.this, "Week", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserWeek = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.CalendarWeek.class);
                startActivity(takeUserWeek);
            }
        });

        bday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarDay.this, "Day", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserDay = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserDay);
            }
        });

        populateDayList();
        populateListView();
        registerClickCallback();
    }

    private void populateDayList() {
        myDay.add(new WeekList("00:00","","","",""));
        myDay.add(new WeekList("01:00","","","",""));
        myDay.add(new WeekList("02:00","","","",""));
        myDay.add(new WeekList("03:00","","","",""));
        myDay.add(new WeekList("04:00","","","",""));
        myDay.add(new WeekList("05:00","","","",""));
        myDay.add(new WeekList("06:00","","","",""));
        myDay.add(new WeekList("07:00","","","",""));
        myDay.add(new WeekList("08:00","","","",""));
        myDay.add(new WeekList("09:00","","","",""));
        myDay.add(new WeekList("10:00","","","",""));
        myDay.add(new WeekList("11:00","","","",""));
        myDay.add(new WeekList("12:00","","","",""));
        myDay.add(new WeekList("13:00","","","",""));
        myDay.add(new WeekList("14:00","","","",""));
        myDay.add(new WeekList("15:00","","","",""));
        myDay.add(new WeekList("16:00","","Group 95 - CS553","E105",""));
        myDay.add(new WeekList("17:00","","Group 95 - CS553","E105",""));
        myDay.add(new WeekList("18:00","","Group 95 - CS553","E105",""));
        myDay.add(new WeekList("19:00","","Group 95 - CS553","E105",""));
        myDay.add(new WeekList("20:00","","","",""));
        myDay.add(new WeekList("21:00","","","",""));
        myDay.add(new WeekList("22:00","","","",""));
        myDay.add(new WeekList("23:00","","","",""));

    }

    private void populateListView() {
        ArrayAdapter<WeekList> notAdapter = new MyDayListAdapter();

        ListView list = (ListView)findViewById(R.id.weekDay);
        list.setAdapter(notAdapter);
    }

    private class MyDayListAdapter extends  ArrayAdapter<WeekList>{

        public  MyDayListAdapter(){
            super(CalendarDay.this, R.layout.weekitem, myDay);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.weekitem, parent, false);
            }

            //find the notification
            WeekList currentWeek = myDay.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.textDayOfWeek);
            makeTextProject.setText(Html.fromHtml(currentWeek.getDayOfWeek()));

            TextView makeTextNum = (TextView) itemView.findViewById(R.id.textNum);
            makeTextNum.setText(Html.fromHtml(currentWeek.getNum()));

            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.textViewTeam);
            makeTextGroup.setText(Html.fromHtml(currentWeek.getTeam()));
            TextView makeTextTime = (TextView) itemView.findViewById(R.id.textViewTime);
            makeTextTime.setText(Html.fromHtml(currentWeek.getTime()));
            TextView makeTextPlace = (TextView) itemView.findViewById(R.id.textViewPlace);
            makeTextPlace.setText(Html.fromHtml(currentWeek.getPlace()));

            return itemView;
        }
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.weekDay);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserMeeting = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.MeetingDetailsFixed.class);
                startActivity(takeUserMeeting);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar_day, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(CalendarDay.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
