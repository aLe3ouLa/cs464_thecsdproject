package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class Search extends ActionBarActivity {
    private List<Project> myProjects = new ArrayList<>();
    private List<People> myPeople = new ArrayList<>();

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons' Listeners
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(Search.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(Search.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(Search.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(Search.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
               // Toast.makeText(Search.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(Search.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(Search.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(Search.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        populateProjectList();
        populatePeopleList();
        populateListView();
        registerClickCallback();
    }

    private void populateProjectList() {
        myProjects.add(new Project("Search Engine", "CS463 - Information Retrieval", "1/2", R.drawable.ic_stick));
        myProjects.add(new Project("User Interface Implementation", "CS464 - Human-Computer Interaction", "2/2", R.drawable.ic_stick));
        myProjects.add(new Project("Mailing Server", "CS335 - Computer Networks", "1/2", R.drawable.ic_stick));

    }
    private void populatePeopleList() {
        myPeople.add(new People("Barka Alexandra", R.drawable.female));
        myPeople.add(new People("Georgiou Giorgos", R.drawable.female));
    }

    private void populateListView() {

        ArrayAdapter<Project> notAdapter = new MyProjectListAdapter();
        ArrayAdapter<People> notAdapter2 = new MyPeopleListAdapter();
        ListView list = (ListView)findViewById(R.id.projectList);
        ListView list2 = (ListView)findViewById(R.id.peopleList);
        list.setAdapter(notAdapter);
        list2.setAdapter(notAdapter2);
    }

    private class MyProjectListAdapter extends  ArrayAdapter<Project>{

        public  MyProjectListAdapter(){
            super(Search.this, R.layout.projectitem, myProjects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.projectitem, parent, false);
            }

            //find the notification
            Project currentProject = myProjects.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextProject.setText(Html.fromHtml(currentProject.getProjectName()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.pitem_day);
            makeTextCourse.setText(Html.fromHtml(currentProject.getCourseName()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentProject.getNumOfPeople()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentProject.getIconID());


            return itemView;
        }
    }

    private class MyPeopleListAdapter extends  ArrayAdapter<People>{

        public  MyPeopleListAdapter() {
            super(Search.this, R.layout.peopleitem, myPeople);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.peopleitem, parent, false);
            }

            //find the notification
            People currentPerson = myPeople.get(position);

            //fill the view
            TextView makeTextPerson = (TextView) itemView.findViewById(R.id.profile_name);
            makeTextPerson.setText(Html.fromHtml(currentPerson.getName()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_personIcon);
            makeView.setImageResource(currentPerson.getIconID());

            return itemView;
        }
    }

    private void registerClickCallback() {
        ListView list = (ListView)findViewById(R.id.peopleList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProfile = new Intent(Search.this, ProfileComplete.class);
                startActivity(takeUserProfile);
            }
        });

        ListView list2 = (ListView)findViewById(R.id.projectList);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(Search.this, ProjectDetails.class);
                startActivity(takeUserProject);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(Search.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(Search.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
