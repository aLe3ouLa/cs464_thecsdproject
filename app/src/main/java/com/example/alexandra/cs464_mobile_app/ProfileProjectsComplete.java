package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ProfileProjectsComplete extends ActionBarActivity {

    private List<GroupD> MyGroup = new ArrayList<>();

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_projects_complete);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(ProfileProjectsComplete.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProfileProjectsComplete.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(ProfileProjectsComplete.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(ProfileProjectsComplete.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(ProfileProjectsComplete.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProfileProjectsComplete.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(ProfileProjectsComplete.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProfileProjectsComplete.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        prepareListData();
        populateListView();
        registerClickCallback();
    }

    private void prepareListData() {
        MyGroup.add(new GroupD(R.drawable.malesmall,"Nikolaou Stathis","nikolst@csd.uoc.gr"));
        MyGroup.add(new GroupD(R.drawable.femalesmall,"Pappa Anna","anpap@csd.uoc.gr"));
        MyGroup.add(new GroupD(R.drawable.femalesmall,"Georgiou Giorgos","georgiou@csd.uoc.gr"));

    }

    private void populateListView() {
        ArrayAdapter<GroupD> notAdapter = new MyGroupListAdapter();
        ListView list = (ListView) findViewById(R.id.groupList);
        list.setAdapter(notAdapter);

    }

    private class MyGroupListAdapter extends ArrayAdapter<GroupD>{
        public  MyGroupListAdapter(){
            super(ProfileProjectsComplete.this, R.layout.smallitem, MyGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.smallitem, parent, false);
            }

            //find the member
            GroupD currentGroup = MyGroup.get(position);

            //fill the view
            ImageView makeView = (ImageView)itemView.findViewById(R.id.isAdmin);
            makeView.setImageResource(currentGroup.getIconPeople());

            TextView makeTextWho = (TextView)itemView.findViewById(R.id.item_name);
            makeTextWho.setText(Html.fromHtml(currentGroup.getWho()));

            TextView makeTextMail = (TextView)itemView.findViewById(R.id.mail);
            makeTextMail.setText(Html.fromHtml(currentGroup.getMail()));

            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.groupList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(ProfileProjectsComplete.this, ProfileComplete.class);
                startActivity(takeUserProject);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_projects_complete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {
        Intent takeUserCal = new Intent(ProfileProjectsComplete.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProfileProjectsComplete.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}