package com.example.alexandra.cs464_mobile_app;

/**
 * Created by Alexandra on 25/04/2015.
 */
public class Notifications {

    private String NotificationText;
    private int iconID;
    private boolean showIcon;
    private String time;



    public Notifications(String notificationText, int iconID, boolean showIcon, String time) {
        NotificationText = notificationText;
        this.iconID = iconID;
        this.showIcon = showIcon;
        this.time = time;
    }

    public String getNotificationText() {
        return NotificationText;
    }

    public void setNotificationText(String notificationText) {
        NotificationText = notificationText;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }

    public boolean isShowIcon() {
        return showIcon;
    }

    public void setShowIcon(boolean showIcon) {
        this.showIcon = showIcon;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}
