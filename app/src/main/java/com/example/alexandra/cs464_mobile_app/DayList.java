package com.example.alexandra.cs464_mobile_app;

/**
 * Created by Eirini on 04-May-15.
 */
public class DayList {

    private String hourInDay;

    public DayList(String hourInDay) {
        this.hourInDay=hourInDay;

    }

    public String getHourInDay() {
        return hourInDay;
    }

    public void setHourInDay(String hourInDay) {
        this.hourInDay = getHourInDay();
    }


}
