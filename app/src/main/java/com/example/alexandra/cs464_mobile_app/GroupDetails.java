package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class GroupDetails extends ActionBarActivity {

    private Button comments;
    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private List<GroupD> MyGroup = new ArrayList<>();
    private List<GroupD> Requests = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_details);

        comments=(Button)findViewById(R.id.button3);
        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupDetails.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupDetails.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupDetails.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(GroupDetails.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(GroupDetails.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(GroupDetails.this, "Comments", Toast.LENGTH_LONG).show();

                //take user to Comments
                Intent takeUserComments = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.GroupDetailsComments.class); //Kanonikos Kwdikas
                startActivity(takeUserComments);
            }
        });

        prepareListData();
        populateListView();
        registerClickCallback();
    }

    private void prepareListData() {
        MyGroup.add(new GroupD(R.drawable.malesmall,"Barka Alexandra","barka@csd.uoc.gr"));
        MyGroup.add(new GroupD(R.drawable.femalesmall,"Degleri Eirini","degleri@csd.uoc.gr"));
        MyGroup.add(new GroupD(R.drawable.blackbigadd,"Invite more members"," "));

        Requests.add(new GroupD(R.drawable.femalesmall,"Chadini Eleni","chadini@csd.uoc.gr"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_group_details, menu);
        return true;
    }

    private void populateListView() {
        ArrayAdapter<GroupD> notAdapter = new MyGroupListAdapter();
        ListView list = (ListView) findViewById(R.id.listMembers);
        list.setAdapter(notAdapter);

        ArrayAdapter<GroupD> notAdapter2 = new RequestsListAdapter();
        ListView list2 = (ListView) findViewById(R.id.listRequests);
        list2.setAdapter(notAdapter2);

    }

    private class MyGroupListAdapter extends ArrayAdapter<GroupD>{
        public  MyGroupListAdapter(){
            super(GroupDetails.this, R.layout.smallitem, MyGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.smallitem, parent, false);
            }

            //find the member
            GroupD currentGroup = MyGroup.get(position);

            //fill the view
            ImageView makeView = (ImageView)itemView.findViewById(R.id.isAdmin);
            makeView.setImageResource(currentGroup.getIconPeople());

            TextView makeTextWho = (TextView)itemView.findViewById(R.id.item_name);
            makeTextWho.setText(Html.fromHtml(currentGroup.getWho()));

            TextView makeTextMail = (TextView)itemView.findViewById(R.id.mail);
            makeTextMail.setText(Html.fromHtml(currentGroup.getMail()));

            return itemView;
        }
    }
    private class RequestsListAdapter extends ArrayAdapter<GroupD>{
        public  RequestsListAdapter(){
            super(GroupDetails.this, R.layout.smallitem, Requests);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.smallitem, parent, false);
            }

            //find the member
            GroupD currentGroup = Requests.get(position);

            //fill the view
            ImageView makeView = (ImageView)itemView.findViewById(R.id.isAdmin);
            makeView.setImageResource(currentGroup.getIconPeople());

            TextView makeTextWho = (TextView)itemView.findViewById(R.id.item_name);
            makeTextWho.setText(Html.fromHtml(currentGroup.getWho()));

            TextView makeTextMail = (TextView)itemView.findViewById(R.id.mail);
            makeTextMail.setText(Html.fromHtml(currentGroup.getMail()));

            return itemView;
        }
    }


    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.listMembers);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(GroupDetails.this, ProfileComplete.class);
                startActivity(takeUserProject);
            }
        });

        ListView list2 = (ListView)findViewById(R.id.listRequests);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(GroupDetails.this, ProfileComplete.class);
                startActivity(takeUserProject);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(GroupDetails.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
