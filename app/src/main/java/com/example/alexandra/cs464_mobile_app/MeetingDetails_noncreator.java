package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


public class MeetingDetails_noncreator extends ActionBarActivity {

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private Button attend;
    private Button decline;

    private ImageButton mSendBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_details_noncreator);

        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        attend = (Button)findViewById(R.id.attendBtn);
        decline = (Button)findViewById(R.id.declineBtn);

        mSendBtn = (ImageButton)findViewById(R.id.sendBtn);

        mSendBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                Intent takeUserMeet = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.MeetingDetails.class);
                startActivity(takeUserMeet);
            }
        });

        attend.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                Intent takeUserMeet = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.MeetingList_Fixed.class);
                startActivity(takeUserMeet);
            }
        });

        decline.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                Intent takeUserMeet = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.MeetingList.class);
                startActivity(takeUserMeet);
            }
        });


        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingDetails_noncreator.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingDetails_noncreator.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingDetails_noncreator.this,"Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
              //  Toast.makeText(MeetingDetails_noncreator.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(MeetingDetails_noncreator.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_meeting_details_noncreator, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(MeetingDetails_noncreator.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }

}



