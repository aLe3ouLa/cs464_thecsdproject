package com.example.alexandra.cs464_mobile_app;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


public class ProfileActivity extends ActionBarActivity {

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private ImageButton mContact;

    private List<fprojects> MyGroup = new ArrayList<>();
    private List<Group> myGroup = new ArrayList<>();

    private int m=1,t=1,w=1,tt=1,f=1,s=1,ss=1;

    private EditText timetext;
    private ImageButton ibTime;
    private Button mondays,tuesdays,wednesdays,thursdays,fridays,saturdays,sundays;
    private Button comments;
    final Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);


        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        //Day of the week button
        mondays = (Button)findViewById(R.id.monday);
        tuesdays = (Button)findViewById(R.id.tuesday);
        wednesdays = (Button)findViewById(R.id.wednesday);
        thursdays = (Button)findViewById(R.id.thursay);
        fridays = (Button)findViewById(R.id.friday);
        saturdays = (Button)findViewById(R.id.saturday);
        sundays = (Button)findViewById(R.id.sunday);

        mondays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (m%2==1){ mondays.setTextColor(Color.BLACK); m++; }
                else { mondays.setTextColor(Color.WHITE); m++; }
            }
        });
        tuesdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (t%2==1){tuesdays.setTextColor(Color.BLACK); t++; }
                else {tuesdays.setTextColor(Color.WHITE);t++;}
            }
        });
        wednesdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (w%2==1){wednesdays.setTextColor(Color.BLACK); w++; }
                else {wednesdays.setTextColor(Color.WHITE);w++;}
            }
        });
        thursdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tt%2==1){thursdays.setTextColor(Color.BLACK); tt++; }
                else {thursdays.setTextColor(Color.WHITE);tt++;}
            }
        });
        fridays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (f%2==1){fridays.setTextColor(Color.BLACK); f++; }
                else {fridays.setTextColor(Color.WHITE);f++;}
            }
        });
        saturdays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (s%2==1){saturdays.setTextColor(Color.BLACK); s++; }
                else {saturdays.setTextColor(Color.WHITE);s++;}
            }
        });
        sundays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ss%2==1){sundays.setTextColor(Color.BLACK); ss++; }
                else {sundays.setTextColor(Color.WHITE);ss++;}
            }

        });
        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProfileActivity.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProfileActivity.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProfileActivity.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(ProfileActivity.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProfileActivity.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProfileActivity.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(ProfileActivity.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProfileActivity.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        comments =(Button)findViewById(R.id.button3);
        comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent takeUserNotif = new Intent(ProfileActivity.this,ProfileCommentsMyView.class);
                startActivity(takeUserNotif);
            }
        });


        prepareListData();
        populateListView();
        registerClickCallback();

        //Profile Meeting Details Buttons
        timetext = (EditText) findViewById(R.id.editTime);
        ibTime = (ImageButton) findViewById(R.id.buttonTime);
        SetCurrentTimeOnView();
    }



    private void prepareListData() {
        MyGroup.add(new fprojects("Compiler implementation","CS340 - Compilers",
                "Spring 2011-2012", R.drawable.eye));
        MyGroup.add(new fprojects("User Interface Implementation","CS464 - Human-Computer Interaction",
                "Spring 2011-2012", R.drawable.eyehidden));

        myGroup.add(new Group(R.drawable.rect_g,"Group23 - CS340","Compilers","3/3","Pappa Anna, Georgiou Giorgos & 1 more",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));
        myGroup.add(new Group(R.drawable.rect_p,"Group14 - CS464","Human-Computer Interaction","2/3","Papadopoulos Paulos, Georgiou Giorgos",
                R.drawable.ic_stick,R.drawable.hourglass,"1",R.drawable.mail,"3","",R.drawable.ic_star_full));
    }

    private void populateListView() {
        ArrayAdapter<fprojects> notAdapter = new projectListAdapter();
        ListView list = (ListView)findViewById(R.id.projectList);
        list.setAdapter(notAdapter);

        ArrayAdapter<Group> notAdapter2 = new MyGroupListAdapter();
        ListView list2 = (ListView)findViewById(R.id.groupList);
        list2.setAdapter(notAdapter2);

    }

    private class projectListAdapter extends ArrayAdapter<fprojects>{
        public  projectListAdapter(){
            super(ProfileActivity.this, R.layout.fprojectitem, MyGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.fprojectitem, parent, false);
            }

            //find the notification
            fprojects currentGroup = MyGroup.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProject()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.pitem_spring);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getWhen()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_day);
            makeNum.setText(Html.fromHtml(currentGroup.getLesson()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.item_icon);
            makeView.setImageResource(currentGroup.getVisisble());

            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.projectList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(ProfileActivity.this, ProfileProject.class);
                startActivity(takeUserProject);
            }
        });

        ListView list2 = (ListView)findViewById(R.id.groupList);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position==0) {
                    Intent takeUserProject = new Intent(ProfileActivity.this, GroupDetailsDone.class);
                    startActivity(takeUserProject);
                }else{
                    Intent takeUserProject = new Intent(ProfileActivity.this, GroupDetails.class);
                    startActivity(takeUserProject);
                }

            }
        });

    }
    private class MyGroupListAdapter extends ArrayAdapter<Group>{
        public  MyGroupListAdapter(){
            super(ProfileActivity.this, R.layout.groupitem, myGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = myGroup.get(position);

            //fill the view
            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());

            return itemView;
        }
    }




    public void SetCurrentTimeOnView(){
        String timeformat ="hh:mm a";
        SimpleDateFormat stf = new SimpleDateFormat(timeformat, Locale.getDefault());
        timetext.setText(stf.format(c.getTime()));
    }


    TimePickerDialog.OnTimeSetListener time = new TimePickerDialog.OnTimeSetListener(){

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            c.set(Calendar.HOUR, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            SetCurrentTimeOnView();
        }
    };

    public void TimeOnClick (View v){
        new TimePickerDialog(ProfileActivity.this,time,c.get(Calendar.HOUR),
                c.get(Calendar.MINUTE),false).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(ProfileActivity.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProfileActivity.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
