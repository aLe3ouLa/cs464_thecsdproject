package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class ProjectNonPassed extends ActionBarActivity {

    private List<Project> myProjects = new ArrayList<>();
    private ImageButton mFilterBtn;
    private ImageButton mOrderBtn;

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_non_passed);

        mFilterBtn = (ImageButton)findViewById(R.id.filterBtn);
        registerForContextMenu(mFilterBtn);
        mFilterBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {
                //To register the button with context menu.
                v.showContextMenu();
                //openContextMenu(mFilterBtn);

            }
        });

        mOrderBtn = (ImageButton)findViewById(R.id.orderBtn);
        registerForContextMenu(mOrderBtn);
        mOrderBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {
                //To register the button with context menu.
                v.showContextMenu();

                //openContextMenu(mFilterBtn);

            }
        });
        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons

        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectNonPassed.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home

                Intent takeUserHome = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectNonPassed.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
               // Toast.makeText(ProjectNonPassed.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(ProjectNonPassed.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProjectNonPassed.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });


        populateProjectList();
        populateListView();

        registerClickCallback();
    }

    private void populateProjectList() {

        myProjects.add(new Project("Search Engine", "CS463 - Information Retrieval", "1/2", R.drawable.ic_stick));
        myProjects.add(new Project("Application Using Web Technologies", "CS359 - Web Programming", "2/2", R.drawable.ic_stick));
        myProjects.add(new Project("Compiler Implementation", "CS340 - Compilers", "3/3", R.drawable.ic_stick));
        myProjects.add(new Project("User Interface Implementation", "CS464 - Human-Computer Interaction", "2/2", R.drawable.ic_stick));

    }

    private void populateListView() {

        ArrayAdapter<Project> notAdapter = new MyProjectListAdapter();

        ListView list = (ListView)findViewById(R.id.projectList);
        list.setAdapter(notAdapter);

    }

    private class MyProjectListAdapter extends  ArrayAdapter<Project>{

        public  MyProjectListAdapter(){
            super(ProjectNonPassed.this, R.layout.projectitem, myProjects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.projectitem, parent, false);
            }

            //find the notification
            Project currentProject = myProjects.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextProject.setText(Html.fromHtml(currentProject.getProjectName()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.pitem_day);
            makeTextCourse.setText(Html.fromHtml(currentProject.getCourseName()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentProject.getNumOfPeople()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentProject.getIconID());

            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.projectList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserProject = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectDetails.class);
                startActivity(takeUserProject);
            }
        });


    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo){


        super.onCreateContextMenu(menu, view, menuInfo);
        switch (view.getId()) {
            case R.id.filterBtn:
                menu.setHeaderTitle("Filter by: ");
                menu.add("Passed Courses");
                menu.add("Non-Passed Courses");
                menu.add("No Filter");
                break;
            case R.id.orderBtn:
                menu.setHeaderTitle("Order by: ");
                menu.add("Code Ascending");
                menu.add("Code Descending");
                menu.add("No order");
                break;
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="Passed Courses"){
            goToPassedCourses(item.getItemId());
        }else if(item.getTitle()=="Non-Passed Courses"){
            goToNonPassedCourses(item.getItemId());
        } else if (item.getTitle()=="No Filter"){
            goToNonFiltered(item.getItemId());
        }else if (item.getTitle()=="Code Ascending"){
            goToAscOrder(item.getItemId());
        }else if (item.getTitle()=="Code Descending"){
            goToDescOrder(item.getItemId());
        }else if (item.getTitle()=="No order"){
            goToNonFiltered(item.getItemId());
        }else{
            return false;
        }
        return true;
    }

    private void goToDescOrder(int itemId) {
        Intent takeUserDesc= new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectListDesc.class);
        startActivity(takeUserDesc);

    }

    private void goToAscOrder(int itemId) {
        Intent takeUserAsc= new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectListAsc.class);
        startActivity(takeUserAsc);

    }

    private void goToNonFiltered(int itemId) {
        Intent takeUserNonFilt= new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectList.class);
        startActivity(takeUserNonFilt);

    }

    private void goToNonPassedCourses(int itemId) {
        Intent takeUserPassed = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectNonPassed.class);
        startActivity(takeUserPassed);
    }

    private void goToPassedCourses(int itemId) {
        Intent takeUserPassed = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.ProjectPassed.class);
        startActivity(takeUserPassed);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_project_non_passed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProjectNonPassed.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
