package com.example.alexandra.cs464_mobile_app;

/**
 * Created by Eirini on 04-May-15.
 */
public class WeekList {
    private String dayOfWeek;
    private String num;
    private String team;
    private String time;
    private String place;

    public WeekList(String dayOfWeek,String num, String team,String time,String place) {
        this.num=num;
        this.dayOfWeek = dayOfWeek;
        this.time=time;
        this.place=place;
        this.team=team;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }
    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = getDayOfWeek();
    }

    public String getNum() {
        return num;
    }
    public void setNum(String num) {
        this.num = getNum();
    }

    public String getPlace() {
        return place;
    }
    public void setPlace(String place) {
        this.place = getPlace();
    }

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = getTime();
    }

    public String getTeam() {
        return team;
    }
    public void setTeam(String team) {
        this.team = getTeam();
    }


}