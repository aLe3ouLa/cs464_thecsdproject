package com.example.alexandra.cs464_mobile_app;


public class Group {
        private int whichRect;
        private String name;
        private String projectTitle;
        private String numOfMembers;
        private String groupMembers;
        private int iconPending;
        private int iconPeople;
        private int iconMessage;
        private String numMessage;
        private String numPending;
        private String isDenied;
        private int  star;

        public Group (int whichRect,String name, String projectTitle, String numOfMembers, String groupMembers,
                      int iconPeople, int iconPending, String numPending,int iconMessage,String numMessage,String isDenied, int star){
            this.whichRect=whichRect;
            this.name = name;
            this.groupMembers = groupMembers;
            this.numOfMembers = numOfMembers;
            this.projectTitle = projectTitle;
            this.iconPeople = iconPeople;
            this.iconPending = iconPending;
            this.iconMessage = iconMessage;
            this.numMessage = numMessage;
            this.numPending = numPending;
            this.isDenied = isDenied;
            this.star = star;
        }

    public Group (int whichRect, String projectTitle, String numOfMembers,int iconPending, String numPending,int iconMessage, String  numMessage ){
        this.whichRect=whichRect;
        this.numOfMembers = numOfMembers;
        this.projectTitle = projectTitle;
        this.numMessage = numMessage;
        this.numPending = numPending;
        this.iconPending = iconPending;
        this.iconMessage = iconMessage;
    }


    int getWhichRect(){return this.whichRect;}
        void setWhichRect(int whichRect){this.whichRect=whichRect;}

        String getName(){ return this.name; }
        void setName(String name){
            this.name = name;
        }

        String getProjectTitle (){
            return this.projectTitle;
        }
        void setProjectTitle(String projectTitle){
            this.projectTitle = projectTitle;
        }

        String getGroupMembers(){
            return this.groupMembers;
        }
        void setGroupMembers(String groupMembers){
            this.groupMembers = groupMembers;
        }

        String getNumOfMembers(){
            return numOfMembers;
        }
        void setNumOfMembers(String numOfMembers){ this.numOfMembers = getNumOfMembers(); }

        int getIconPeople(){
            return iconPeople;
        }
        void setIconPeople(int iconPeople){this.iconPeople = getIconPeople();}

        int getIconPending(){
            return iconPending;
        }
        void setIconPending(int iconPending){this.iconPending = getIconPending();}

        int getIconMessage(){
            return iconMessage;
        }
        void setIconMessage(int iconMessage){this.iconMessage = getIconMessage();}

        String getNumPending(){
            return numPending;
        }
        void setNumPending(String numPending){this.numPending = getNumPending();}

        String getNumMessage(){
            return numMessage;
        }
        void setNumMessage(String numMessage){this.numMessage = getNumMessage();}

        String getIsDenied(){
            return isDenied;
        }
        void setIsDenied(String isDenied){this.isDenied = getIsDenied();}

        int getStar(){
        return star;
    }
        void setStar(int star){this.star = getStar();}
    }
