package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class CreateNewMeeting extends ActionBarActivity implements AdapterView.OnItemSelectedListener{

    private Button mCreateMeetingBtn;
    private Spinner mGroupCode,mPlace, mDate,mTime;
    private EditText mComment;

    private List<Meeting>meetingList = new ArrayList<>();

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_meeting);

        mCreateMeetingBtn = (Button)findViewById(R.id.createTeamBtn);
        mGroupCode = (Spinner)findViewById(R.id.spinnerGroup);
        mPlace = (Spinner)findViewById(R.id.spinnerPlace);
        mTime = (Spinner)findViewById(R.id.spinnerTime);
        mDate = (Spinner)findViewById(R.id.spinnerDate);
        mComment = (EditText)findViewById(R.id.editComment);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);


        /*Spinner for PGroups*/
        ArrayAdapter agroups=ArrayAdapter.createFromResource(this,R.array.groups,android.R.layout.simple_spinner_item);
        mGroupCode.setAdapter(agroups);
        mGroupCode.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        /*Spinner for Places*/
        ArrayAdapter aplaces=ArrayAdapter.createFromResource(this,R.array.places,android.R.layout.simple_spinner_item);
        mPlace.setAdapter(aplaces);
        mPlace.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
         /*Spinner for Days*/
        ArrayAdapter adates=ArrayAdapter.createFromResource(this,R.array.dates,android.R.layout.simple_spinner_item);
        mDate.setAdapter(adates);
        mDate.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
         /*Spinner for Times*/
        ArrayAdapter atimes=ArrayAdapter.createFromResource(this,R.array.times,android.R.layout.simple_spinner_item);
        mTime.setAdapter(atimes);
        mTime.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);



        mCreateMeetingBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                Toast.makeText(CreateNewMeeting.this, "Meeting Successfully Created!", Toast.LENGTH_LONG).show();

                /* Create group somehow */

                String Comment = mComment.getText().toString().trim();


                //take user to Home
                Intent takeUserMeetings = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.MeetingList.class);
                startActivity(takeUserMeetings);
            }
        });

        // Action Bar Buttons
       mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CreateNewMeeting.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(CreateNewMeeting.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
               // Toast.makeText(CreateNewMeeting.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(CreateNewMeeting.this, CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(CreateNewMeeting.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to Notifications

                Intent takeUserNotification = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.NotificationsListView.class);
                startActivity(takeUserNotification);
            }
        });
    }


    public void onItemSelected(AdapterView<?> adapterView,View view,int i ,long l){
        TextView myText=(TextView) view;
       // Toast.makeText(this,"You selected "+myText.getText(), Toast.LENGTH_SHORT).show();
    }

    public void onNothingSelected(AdapterView<?> adapterView){

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_create_new_meeting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(CreateNewMeeting.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
