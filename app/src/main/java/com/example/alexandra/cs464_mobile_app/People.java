package com.example.alexandra.cs464_mobile_app;



/**
 * Created by Eirini on 26-Apr-15.
 */
public class People {

    private String Name;
    private int iconID;

    public People(String Name, int iconID) {
        this.Name = Name;
        this.iconID = iconID;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }


}
