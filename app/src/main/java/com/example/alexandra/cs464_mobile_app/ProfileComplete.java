package com.example.alexandra.cs464_mobile_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ProfileComplete extends ActionBarActivity {

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private Button comments;

    private ImageButton mContact;

    private List<fprojects> MyGroup = new ArrayList<>();
    private List<Group> myGroup = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_complete);

        mContact = (ImageButton)findViewById(R.id.imageButton);
        mContact.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                showPopUp2();
            }
        });

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProfileComplete.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProfileComplete.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(ProfileComplete.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(ProfileComplete.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(ProfileComplete.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProfileComplete.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(ProfileComplete.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProfileComplete.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        comments =(Button)findViewById(R.id.button3);
        comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent takeUserNotif = new Intent(ProfileComplete.this,ProfileComments.class);
                startActivity(takeUserNotif);
            }
        });


        prepareListData();
        populateListView();
        registerClickCallback();
    }

    private void showPopUp2() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Send Mail:");
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.maillayout, null);
        helpBuilder.setView(dialoglayout);
        helpBuilder.setPositiveButton("Send",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        Toast.makeText(ProfileComplete.this, "E-mail Sent!", Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

        helpBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }


    private void prepareListData() {
        MyGroup.add(new fprojects("Compiler implementation","CS340 - Compilers",
                "Spring 2011-2012", R.drawable.white2));

        myGroup.add(new Group(R.drawable.rect_g,"Group23 - CS340","Compilers","3/3","Pappa Anna, Georgiou Giorgos & 1 more",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));
        myGroup.add(new Group(R.drawable.rect_p,"Group14 - CS464","Human-Computer Interaction","2/3","Papadopoulos Paulos, Georgiou Giorgos",
                R.drawable.ic_stick,R.drawable.hourglass,"1",R.drawable.mail,"3","",R.drawable.ic_star_full));
    }

    private void populateListView() {
        ArrayAdapter<fprojects> notAdapter = new projectListAdapter();
        ListView list = (ListView)findViewById(R.id.projectList);
        list.setAdapter(notAdapter);

        ArrayAdapter<Group> notAdapter2 = new MyGroupListAdapter();
        ListView list2 = (ListView)findViewById(R.id.groupList);
        list2.setAdapter(notAdapter2);
    }

    private class projectListAdapter extends ArrayAdapter<fprojects>{
        public  projectListAdapter(){
            super(ProfileComplete.this, R.layout.fprojectitem, MyGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.fprojectitem, parent, false);
            }

            //find the notification
            fprojects currentGroup = MyGroup.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProject()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.pitem_spring);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getWhen()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_day);
            makeNum.setText(Html.fromHtml(currentGroup.getLesson()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.item_icon);
            makeView.setImageResource(currentGroup.getVisisble());

            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.projectList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent takeUserProject = new Intent(ProfileComplete.this, ProfileProjectsComplete.class);
                startActivity(takeUserProject);
            }
        });

        ListView list2 = (ListView)findViewById(R.id.groupList);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0) {
                    Intent takeUserProject = new Intent(ProfileComplete.this, GroupDetailsDone.class);
                    startActivity(takeUserProject);
                }else{
                    Intent takeUserProject = new Intent(ProfileComplete.this, GroupDetails.class);
                    startActivity(takeUserProject);
                }
            }
        });

    }
    private class MyGroupListAdapter extends ArrayAdapter<Group>{
        public  MyGroupListAdapter(){
            super(ProfileComplete.this, R.layout.groupitem, myGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = myGroup.get(position);

            //fill the view
            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());

            return itemView;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_complete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(ProfileComplete.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProfileComplete.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}

