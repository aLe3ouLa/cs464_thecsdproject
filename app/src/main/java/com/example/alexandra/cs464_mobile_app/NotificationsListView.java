package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class NotificationsListView extends ActionBarActivity {

private List<Notifications> myNotifications = new ArrayList<>();

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons

        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(NotificationsListView.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(NotificationsListView.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(NotificationsListView.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(NotificationsListView.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
               // Toast.makeText(NotificationsListView.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(NotificationsListView.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
              //  Toast.makeText(NotificationsListView.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(NotificationsListView.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });


        populateNotificationsList();
        populateListView();

        registerClickCallback();


    }


    private void populateNotificationsList() {

        myNotifications.add(new Notifications("<b>Chadini Eleni</b> accepted invitation to <b><font color=#395998>Group 01 - CS340</font></b>", R.drawable.dot, true, "2 min ago"));
        myNotifications.add(new Notifications("<b><font color=#395998>Group 01 - CS340</font></b> is now Complete", R.drawable.dot, true, "4 min ago" ));
        myNotifications.add(new Notifications("<b>Degleri Eirini-Aikaterini</b> left <b><font color=#395998>Group 02 - CS464</font></b>", R.drawable.dot, true, "5 min ago" ));
        myNotifications.add(new Notifications("<b>Degleri Eirini-Aikaterini</b> commented to  <b><font color=#395998>Group 02 - CS464</font></b>", R.drawable.whitedot, false, "1 hr ago" ));
        myNotifications.add(new Notifications("<b>Georgiou Giorgos</b> has changed location for <b><font color=#395998>Group 23 - CS340</font></b>", R.drawable.whitedot, false, "1 hr ago" ));
        myNotifications.add(new Notifications("<b>Georgiou Giorgos</b> has invited you to <b><font color=#395998>Group 23 - CS340</font></b>", R.drawable.whitedot, false, "2 hr ago" ));
        myNotifications.add(new Notifications("<b>Georgiou Giorgos</b> has invited you to <b><font color=#395998>Group 23 - CS340</font></b>", R.drawable.whitedot, false, "2 hr ago" ));

    }


    private void populateListView() {

       ArrayAdapter<Notifications> notAdapter = new MyListAdapter();

        ListView list = (ListView)findViewById(R.id.notificationList);
        list.setAdapter(notAdapter);

    }

    private class MyListAdapter extends  ArrayAdapter<Notifications>{

        public MyListAdapter(){
            super(NotificationsListView.this, R.layout.notificationitem, myNotifications);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.notificationitem, parent, false);
            }

            //find the notification
            Notifications currentNotification = myNotifications.get(position);

            //fill the view
            ImageView imageview = (ImageView)itemView.findViewById(R.id.item_icon);
            imageview.setImageResource(currentNotification.getIconID());

            TextView makeText = (TextView) itemView.findViewById(R.id.item_txtNotification);

            makeText.setText(Html.fromHtml(currentNotification.getNotificationText()));

            TextView makeTextTime = (TextView) itemView.findViewById(R.id.item_txtTime);
            makeTextTime.setText(currentNotification.getTime());



            return itemView;
             }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.notificationList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserProfile = new Intent(NotificationsListView.this, GroupDetails.class);
                startActivity(takeUserProfile);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_notifications, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(NotificationsListView.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(NotificationsListView.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
