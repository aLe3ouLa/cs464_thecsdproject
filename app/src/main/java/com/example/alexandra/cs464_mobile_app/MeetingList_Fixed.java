package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class MeetingList_Fixed extends ActionBarActivity {

    private List<Meeting> myMeetings = new ArrayList<>();
    private Button mfixedBtn;
    private Button mnonFixedBtn;
    private Button createmeet;


    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_list__fixed);

        mfixedBtn = (Button)findViewById(R.id.fixedBtn1);
        mnonFixedBtn = (Button)findViewById(R.id.nonFixedBtn1);

        createmeet = (Button)findViewById(R.id.createTeamBtn);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);


        mfixedBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingList_Fixed.this, "Pushed fixed", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserFixed = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.MeetingList_Fixed.class);
                startActivity(takeUserFixed);
            }
        });

        mnonFixedBtn.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingList_Fixed.this, "Pushed nonfixed", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserNonFixed = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.MeetingList.class);
                startActivity(takeUserNonFixed);
            }
        });

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingList_Fixed.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingList_Fixed.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
              //  Toast.makeText(MeetingList_Fixed.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingList_Fixed.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(MeetingList_Fixed.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        createmeet.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(MeetingList_Fixed.this, CreateNewMeeting.class);
                startActivity(takeUserNotif);
            }
        });

        populateMeetingList();
        populateListView();

        registerClickCallback();


    }

    private void populateMeetingList() {

        myMeetings.add(new Meeting("Group 95 - CS553", "Saturday 8 May 2015 ", "16:00 - 19:00", "E105", R.drawable.dot, R.drawable.dot, R.drawable.whitedot,
                "Georgiou G.", "Degleri E." , "", R.drawable.ic_star_full));
        myMeetings.add(new Meeting("Group 32 - CS335", "Saturday 19 March 2015 ", "10:00 - 12:00", "Starbucks", R.drawable.dot, R.drawable.dot, R.drawable.dot,
                "Georgiou G.", "Barka A." , "Maurou R.", R.drawable.ic_star_empty));
        myMeetings.add(new Meeting("Group 32 - CS335", "Saturday 12 March 2015 ", "10:00 - 12:00", "Study Hall", R.drawable.dot, R.drawable.dot, R.drawable.dot,
                "Georgiou G.", "Barka A." , "Maurou R.", R.drawable.ic_star_empty));
        myMeetings.add(new Meeting("Group 32 - CS335", "Saturday 26 February 2015 ", "10:00 - 12:00", "Study Hall", R.drawable.dot, R.drawable.dot, R.drawable.dot,
                "Georgiou G.", "Barka A." , "Maurou R.", R.drawable.ic_star_empty));
        myMeetings.add(new Meeting("Group 32 - CS335", "Saturday 21 February 2015 ", "10:00 - 12:00", "Study Hall", R.drawable.dot, R.drawable.dot, R.drawable.dot,
                "Georgiou G.", "Barka A." , "Maurou R.", R.drawable.ic_star_empty));

    }

    private void populateListView() {

        ArrayAdapter<Meeting> meetAdapter = new MyMeetingListAdapter();

        ListView list = (ListView)findViewById(R.id.meetingListView1);
        list.setAdapter(meetAdapter);

    }


    private class MyMeetingListAdapter extends  ArrayAdapter<Meeting>{

        public  MyMeetingListAdapter(){
            super(MeetingList_Fixed.this, R.layout.meetingitem, myMeetings);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.meetingitem, parent, false);
            }

            //find the notification
            Meeting currentMeeting = myMeetings.get(position);

            //fill the view
            TextView makeTextCourse = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextCourse.setText(Html.fromHtml(currentMeeting.getCourseName()));

            TextView makeTextDay = (TextView)itemView.findViewById(R.id.pitem_day);
            makeTextDay.setText(Html.fromHtml(currentMeeting.getDay()));

            TextView makeTextTime = (TextView)itemView.findViewById(R.id.pitem_time);
            makeTextTime.setText(Html.fromHtml(currentMeeting.getTime()));

            TextView makeTextPlace = (TextView)itemView.findViewById(R.id.pitem_place);
            makeTextPlace.setText(Html.fromHtml(currentMeeting.getPlace()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentMeeting.getIconID());

            ImageView makeicon1 = (ImageView)itemView.findViewById(R.id.pitem_icon1);
            makeicon1.setImageResource(currentMeeting.getIcon1());

            ImageView makeicon2 = (ImageView)itemView.findViewById(R.id.pitem_icon2);
            makeicon2.setImageResource(currentMeeting.getIcon2());

            ImageView makeicon3 = (ImageView)itemView.findViewById(R.id.pitem_icon3);
            makeicon3.setImageResource(currentMeeting.getIcon3());

            TextView makeTextPerson1 = (TextView)itemView.findViewById(R.id.pitem_person1);
            makeTextPerson1.setText(Html.fromHtml(currentMeeting.getPerson1()));

            TextView makeTextPerson2 = (TextView)itemView.findViewById(R.id.pitem_Person2);
            makeTextPerson2.setText(Html.fromHtml(currentMeeting.getPerson2()));

            TextView makeTextPerson3 = (TextView)itemView.findViewById(R.id.pitem_Person3);
            makeTextPerson3.setText(Html.fromHtml(currentMeeting.getPerson3()));
            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.meetingListView1);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent takeUserMeeting = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.MeetingDetailsFixed.class);
                startActivity(takeUserMeeting);
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_meeting_list__fixed, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(MeetingList_Fixed.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
