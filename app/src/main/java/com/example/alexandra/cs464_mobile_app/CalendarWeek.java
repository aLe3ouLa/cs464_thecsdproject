package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class CalendarWeek extends ActionBarActivity {
    private List<WeekList> myWeek = new ArrayList<>();

    private Button bweek,bmonth,bday;

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_week);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarWeek.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarWeek.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarWeek.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.CalendarView.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarWeek.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(CalendarWeek.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        //Buttons
        bmonth = (Button)findViewById(R.id.month);
        bweek = (Button)findViewById(R.id.week);
        bday = (Button)findViewById(R.id.days);

        // Calendar Buttons
        bmonth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarWeek.this, "Month", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserMonth = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.CalendarView.class);
                startActivity(takeUserMonth);
            }
        });

        bweek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarWeek.this, "Week", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserWeek = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.CalendarWeek.class);
                startActivity(takeUserWeek);
            }
        });

        bday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarWeek.this, "Day", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserDay = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserDay);
            }
        });


        populateWeekList();
        populateListView();
        registerClickCallback();
    }

    private void populateWeekList() {
        myWeek.add(new WeekList("SUN","10","","",""));
        myWeek.add(new WeekList("MON","11","","",""));
        myWeek.add(new WeekList("TUE","12","","",""));
        myWeek.add(new WeekList("WED","13","","",""));
        myWeek.add(new WeekList("THU","14","","",""));
        myWeek.add(new WeekList("FRI","15","Group 95 - CS553","16:00 - 19:00","E105"));
        myWeek.add(new WeekList("SAT","16","","",""));

  /*      myWeek.add(new WeekList("SUN","17"));
        myWeek.add(new WeekList("MON","18"));
        myWeek.add(new WeekList("TUE","19"));
        myWeek.add(new WeekList("WED","20"));
        myWeek.add(new WeekList("THU","21"));
        myWeek.add(new WeekList("FRI","22"));
        myWeek.add(new WeekList("SAT","23"));

        myWeek.add(new WeekList("SUN","24"));
        myWeek.add(new WeekList("MON","25"));
        myWeek.add(new WeekList("TUE","26"));
        myWeek.add(new WeekList("WED","27"));
        myWeek.add(new WeekList("THU","28"));
        myWeek.add(new WeekList("FRI","29"));
        myWeek.add(new WeekList("SAT","30"));
        myWeek.add(new WeekList("SUN","31")); */

    }

    private void populateListView() {
        ArrayAdapter<WeekList> notAdapter = new MyWeekListAdapter();

        ListView list = (ListView)findViewById(R.id.weekList);
        list.setAdapter(notAdapter);

    }

    private class MyWeekListAdapter extends  ArrayAdapter<WeekList>{

        public  MyWeekListAdapter(){
            super(CalendarWeek.this, R.layout.weekitem, myWeek);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.weekitem, parent, false);
            }

            //find the notification
            WeekList currentWeek = myWeek.get(position);

            //fill the view
            TextView makeTextProject = (TextView) itemView.findViewById(R.id.textDayOfWeek);
            makeTextProject.setText(Html.fromHtml(currentWeek.getDayOfWeek()));

            TextView makeTextNum = (TextView) itemView.findViewById(R.id.textNum);
            makeTextNum.setText(Html.fromHtml(currentWeek.getNum()));

            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.textViewTeam);
            makeTextGroup.setText(Html.fromHtml(currentWeek.getTeam()));
            TextView makeTextTime = (TextView) itemView.findViewById(R.id.textViewTime);
            makeTextTime.setText(Html.fromHtml(currentWeek.getTime()));
            TextView makeTextPlace = (TextView) itemView.findViewById(R.id.textViewPlace);
            makeTextPlace.setText(Html.fromHtml(currentWeek.getPlace()));

            return itemView;
        }
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.weekList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserMeeting = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.MeetingDetailsFixed.class);
                startActivity(takeUserMeeting);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar_week, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(CalendarWeek.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
