package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class ProjectDetails extends ActionBarActivity {

    RatingBar rb;

    private Button comments;
    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private TextView description;
    private TextView subDays;
    private Button createGroupBtn;
    private List<Group> myGroup= new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);

        createGroupBtn = (Button)findViewById(R.id.createBtn);
        createGroupBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent takeUserCreateGroup = new Intent(ProjectDetails.this, CreateGroup.class);
                startActivity(takeUserCreateGroup);
            }
        });




        comments=(Button)findViewById(R.id.button3);
        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        String std = "<b>Phase A</b> <br> <br> <b>Requirement Analysis</b> <br>For each of the themes A and B, a requirement \n" +
                "\u0001this analysis of your prefered theme. Search for\n" +
                "systems that has similar features with the requi-\n" +
                "rements that you are asked to implement. <br> <br> <b>Phase B</b> <br> <br> <b>Design </b> <br> Design prototypes of high fidelity, of the theme \n" +
                "you chose in the previous phase. You can either\n" +
                "prototype using Adobe Fireworks or Prototyper.\n";
        description = (TextView)findViewById(R.id.descrTextView);

        description.setText(Html.fromHtml(std));

        String sdstr = "<b>Phase A</b> - Requirements Analysis <br> <font color=#337b22><b>Start:</b></font> 27/02/2015 &emsp<font color=#ff0000><b>Deadline:</b></font> 06/03/2015<br><br>" +
                "<b>Phase B</b> - Design (Intermediate)<br> <font color=#337b22><b>Start:</b></font> 06/03/2015 &emsp<font color=#ff0000><b>Deadline:</b></font> 16/03/2015 <br><br>" +
                "<b>Phase B</b> - Design (Final Submission) <br> <font color=#337b22><b>Start:</b></font> 16/03/2015 &emsp<font color=#ff0000><b>Deadline:</b></font> 27/03/2015<br><br>" +
                "<b>Phase C</b> - Implementation <br> <font color=#337b22><b>Start:</b></font> 27/03/2015 &emsp<font color=#ff0000><b>Deadline:</b></font> 06/04/2015<br><br>" +
                "<b>Phase D</b> - UX <br> <font color=#337b22><b>Start:</b></font> 27/02/2015 &emsp<font color=#ff0000><b>Deadline:</b></font> 06/03/2015 <br><br>";
        subDays = (TextView)findViewById(R.id.subDaysTextView);
        subDays.setText(Html.fromHtml(sdstr));


        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectDetails.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectDetails.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectDetails.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectDetails.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProjectDetails.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        comments.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(ProjectDetails.this, "Comments", Toast.LENGTH_LONG).show();

                //take user to Comments
                Intent takeUserComments = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.ProjectDetailComments.class);
                startActivity(takeUserComments);
            }
        });


        rb=(RatingBar)findViewById(R.id.ratingBar2);

        rb.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {

                Toast.makeText(getApplicationContext(), Float.toString(rating), Toast.LENGTH_LONG).show();

            }

        });

        populateGroupList();
        populateListView();

       registerClickCallback();
    }

    private void populateGroupList() {

        myGroup.add(new Group(R.drawable.rect_p, "Group 01 - CS464", "2",R.drawable.hourglass,"1",R.drawable.mail,"3" ));
        myGroup.add(new Group(R.drawable.rect_p, "Group 02 - CS464", "1",R.drawable.hourglass,"1",R.drawable.mail,"3" ));
        myGroup.add(new Group(R.drawable.rect_g, "Group 03 - CS464", "2",R.drawable.white2,"",R.drawable.white2,"" ));
        myGroup.add(new Group(R.drawable.rect_g, "Group 04 - CS464", "3",R.drawable.white2,"",R.drawable.white2,"" ));
        myGroup.add(new Group(R.drawable.rect_p, "Group 05 - CS464", "1",R.drawable.hourglass,"1",R.drawable.mail,"3" ));
}

    private void populateListView() {

        ArrayAdapter<Group> notAdapter = new MyListAdapter();

        ListView list = (ListView)findViewById(R.id.gitem_list);
        list.setAdapter(notAdapter);

    }

    private class MyListAdapter extends  ArrayAdapter<Group>{

        public MyListAdapter(){
            super(ProjectDetails.this, R.layout.projectdetails, myGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.projectdetails, parent, false);
            }

            //find the notification
            Group currGroup = myGroup.get(position);
            //fill the view
            ImageView imageview = (ImageView)itemView.findViewById(R.id.icon_rect);
            imageview.setImageResource(currGroup.getWhichRect());

            TextView makeText = (TextView) itemView.findViewById(R.id.pitem_name);
            makeText.setText(Html.fromHtml(currGroup.getProjectTitle()));

            //TextView makeTextNumMem = (TextView) itemView.findViewById(R.id.numMem);
            //makeTextNumMem.setText(currGroup.getGroupMembers());
            TextView numberOfPeople =  (TextView)itemView.findViewById(R.id.text);
            numberOfPeople.setText(currGroup.getNumOfMembers());

            TextView makeTextNumPen = (TextView) itemView.findViewById(R.id.numPending);
            makeTextNumPen.setText(currGroup.getNumPending());

            TextView makeTextNumInv = (TextView) itemView.findViewById(R.id.numMessage);
            makeTextNumInv.setText(currGroup.getNumMessage());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currGroup.getIconMessage());


            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.gitem_list);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserGroup = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.GroupDetails.class);
                startActivity(takeUserGroup );
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_project_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProjectDetails.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
