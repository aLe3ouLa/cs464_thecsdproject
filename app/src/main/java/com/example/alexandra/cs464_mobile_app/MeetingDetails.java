package com.example.alexandra.cs464_mobile_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;


public class MeetingDetails extends ActionBarActivity {

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;

    private ImageButton mEditBtn;
    private ImageButton mSendBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_details);

        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        mEditBtn = (ImageButton)findViewById(R.id.editpenBtn);
        mSendBtn = (ImageButton)findViewById(R.id.sendBtn);

        mSendBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                Intent takeUserMeet = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.MeetingDetails.class);
                startActivity(takeUserMeet);
            }
        });

        mEditBtn.setOnClickListener(new android.view.View.OnClickListener() {

            public void onClick(View v) {

                showPopUp2();
            }
        });


        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
              //  Toast.makeText(MeetingDetails.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
              //  Toast.makeText(MeetingDetails.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener(){


            @Override
            public void onClick(View v) {
               // Toast.makeText(MeetingDetails.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(MeetingDetails.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(MeetingDetails.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_meeting_details, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(MeetingDetails.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }

    private void showPopUp2() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Edit:");
        LayoutInflater inflater = getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.popuplayout, null);
        helpBuilder.setView(dialoglayout);
        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        // Do nothing but close the dialog
                        dialog.dismiss();
                    }
                });

        helpBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
            }
        });

        // Remember, create doesn't show the dialog
        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }
}



