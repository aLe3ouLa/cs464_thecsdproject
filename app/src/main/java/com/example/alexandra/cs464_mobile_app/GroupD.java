package com.example.alexandra.cs464_mobile_app;

/**
 * Created by Eirini on 07-May-15.
 */
public class GroupD {
    private int iconPeople;
    private String who;
    private String mail;

    public GroupD (int iconPeople, String who, String mail){
        this.iconPeople = iconPeople;
        this.who = who;
        this.mail = mail;
    }

    int getIconPeople(){
        return iconPeople;
    }
    void setIconPeople(int iconPeople){this.iconPeople = getIconPeople();}


    String getWho(){ return this.who; }
    void setWho(String who){
        this.who = getWho();
    }

    String getMail (){
        return this.mail;
    }
    void setMail(String mail){
        this.mail = getMail();
    }
}
