package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class CalendarView extends ActionBarActivity {
    private Button bweek,bmonth,bday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_view);

        //Buttons
        bmonth = (Button)findViewById(R.id.month);
        bweek = (Button)findViewById(R.id.week);
        bday = (Button)findViewById(R.id.days);

        // Buttons
        bmonth.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Toast.makeText(CalendarView.this, "Month", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserMonth = new Intent(CalendarView.this, com.example.alexandra.cs464_mobile_app.CalendarView.class);
                startActivity(takeUserMonth);
            }
        });

        bweek.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Toast.makeText(CalendarView.this, "Week", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserWeek = new Intent(CalendarView.this, com.example.alexandra.cs464_mobile_app.CalendarWeek.class);
                startActivity(takeUserWeek);
            }
        });

        bday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
              //  Toast.makeText(CalendarView.this, "Day", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserWeek = new Intent(CalendarView.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserWeek);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calendar_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(CalendarView.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(CalendarView.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
