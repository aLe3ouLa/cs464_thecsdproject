package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


public class GroupList extends ActionBarActivity {

    private List<Group> MyGroup = new ArrayList<>();
    private List<Group> Pending = new ArrayList<>();
    private List<Group> Invited = new ArrayList<>();
    private List<Group> History = new ArrayList<>();

    private Button mCreate;

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_list);

        mCreate = (Button)findViewById(R.id.createBtn);

        // Action Bar Buttons
        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);


        mHome.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupList.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupList.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                //Toast.makeText(GroupList.this, "Calendar", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
               // Toast.makeText(GroupList.this, "NotificationsListView", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(GroupList.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        //add listener to button

        mCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //correct credentials
              //  Toast.makeText(GroupList.this, "Create Group", Toast.LENGTH_LONG).show();

                //take user to home page
                Intent takeUserCreate = new Intent(GroupList.this, CreateGroup.class);
                startActivity(takeUserCreate);
            }
        });

        prepareListData();
        populateListView();
        registerClickCallback();

    }

    private void prepareListData() {
        MyGroup.add(new Group(R.drawable.rect_g,"Group01 - CS340","Compilers","3/3","Barka Alexandra, Chadini Eleni & 1 more",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));
        MyGroup.add(new Group(R.drawable.rect_p,"Group02 - CS464","Human-Computer Interaction","2/3","Barka Alexandra, Degleri Eirini ",
                R.drawable.ic_stick,R.drawable.hourglass,"1",R.drawable.mail,"3","",R.drawable.whitedot));
        MyGroup.add(new Group(R.drawable.rect_g,"Group02 - CS463","Information Retrieval","2/2","Barka Alexandra, Degleri Eirini",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));

        Pending.add(new Group(R.drawable.rect_p,"Group12 - CS359","Web Programming","1/2","Barka Alexandra",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));
        Pending.add(new Group(R.drawable.rect_p,"Group16 - CS252","Object Oriented Programming","1/2","Barka Alexandra",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));

        Invited.add(new Group(R.drawable.rect_p,"Group03 - CS464","Human-Computer Interaction","2/3","Nikodemou Vasilis, Poutouris Vangelis",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));
        Invited.add(new Group(R.drawable.rect_p,"Group04 - CS464","Human-Computer Interaction","1/3","Vardakis Giorgos",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","",R.drawable.whitedot));

        History.add(new Group(R.drawable.rect_g,"Group01 - CS340","Compilers","2/3","Barka Alexandra",
                R.drawable.ic_stick,R.drawable.white2," ",R.drawable.white2," ","Denied",R.drawable.whitedot));

    }

    private void populateListView() {
        ArrayAdapter<Group> notAdapter = new MyGroupListAdapter();
        ListView list = (ListView)findViewById(R.id.myGroupList);
        list.setAdapter(notAdapter);

        ArrayAdapter<Group> notAdapter3 = new MyPendingListAdapter();
        ListView list3 = (ListView)findViewById(R.id.invitedList);
        list3.setAdapter(notAdapter3);

        ArrayAdapter<Group> notAdapter2 = new MyInvitedListAdapter();
        ListView list2 = (ListView)findViewById(R.id.pendingList);
        list2.setAdapter(notAdapter2);

        ArrayAdapter<Group> notAdapter4 = new MyHistoryListAdapter();
        ListView list4 = (ListView)findViewById(R.id.historyList);
        list4.setAdapter(notAdapter4);
    }


    private class MyGroupListAdapter extends ArrayAdapter<Group>{
        public  MyGroupListAdapter(){
            super(GroupList.this, R.layout.groupitem, MyGroup);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = MyGroup.get(position);

            //fill the view
            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());

            return itemView;
        }
    }

    private class MyPendingListAdapter extends ArrayAdapter<Group>{
        public  MyPendingListAdapter(){
            super(GroupList.this, R.layout.groupitem, Pending);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = Pending.get(position);

            //fill the view
            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());

            return itemView;
        }
    }

    private class MyInvitedListAdapter extends ArrayAdapter<Group>{
        public  MyInvitedListAdapter(){
            super(GroupList.this, R.layout.groupitem, Invited);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = Invited.get(position);

            //fill the view
            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());

            return itemView;
        }
    }

    private class MyHistoryListAdapter extends ArrayAdapter<Group>{
        public  MyHistoryListAdapter(){
            super(GroupList.this, R.layout.groupitem, History);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View itemView = convertView;

            //Make sure we have a view to work with
            if (itemView == null){
                itemView = getLayoutInflater().inflate(R.layout.groupitem, parent, false);
            }

            //find the notification
            Group currentGroup = History.get(position);

            //fill the view
            ImageView makeView4 = (ImageView)itemView.findViewById(R.id.icon_rect);
            makeView4.setImageResource(currentGroup.getWhichRect());

            TextView makeTextGroup = (TextView) itemView.findViewById(R.id.pitem_name);
            makeTextGroup.setText(Html.fromHtml(currentGroup.getName()));

            TextView makeTextProject = (TextView) itemView.findViewById(R.id.pitem_day);
            makeTextProject.setText(Html.fromHtml(currentGroup.getProjectTitle()));

            TextView makeTextCourse = (TextView)itemView.findViewById(R.id.TeamNames);
            makeTextCourse.setText(Html.fromHtml(currentGroup.getGroupMembers()));

            TextView makeNum = (TextView)itemView.findViewById(R.id.pitem_num);
            makeNum.setText(Html.fromHtml(currentGroup.getNumOfMembers()));

            ImageView makeView = (ImageView)itemView.findViewById(R.id.pitem_star);
            makeView.setImageResource(currentGroup.getIconPeople());

            ImageView makeView2 = (ImageView)itemView.findViewById(R.id.pitem_hourglass);
            makeView2.setImageResource(currentGroup.getIconPending());

            ImageView makeView3 = (ImageView)itemView.findViewById(R.id.pitem_message);
            makeView3.setImageResource(currentGroup.getIconMessage());

            TextView makeTextPending = (TextView)itemView.findViewById(R.id.numPending);
            makeTextPending.setText(Html.fromHtml(currentGroup.getNumPending()));

            TextView makeTextMessage = (TextView)itemView.findViewById(R.id.numMessage);
            makeTextMessage.setText(Html.fromHtml(currentGroup.getNumMessage()));

            TextView makeTextMessage2 = (TextView)itemView.findViewById(R.id.isDenied);
            makeTextMessage2.setText(Html.fromHtml(currentGroup.getIsDenied()));

            ImageView star = (ImageView)itemView.findViewById(R.id.icon);
            star.setImageResource(currentGroup.getStar());
            return itemView;
        }
    }

    private void registerClickCallback() {

        ListView list = (ListView)findViewById(R.id.myGroupList);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0) {
                    Intent takeUserProject = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.GroupDetailsDone.class);
                    startActivity(takeUserProject);
                }
                else {
                    Intent takeUserProject = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.GroupDetails.class);
                    startActivity(takeUserProject);
                }
            }
        });

        ListView list2 = (ListView)findViewById(R.id.invitedList);
        list2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserProject = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.GroupDetails.class);
                startActivity(takeUserProject);
            }
        });

        ListView list3 = (ListView)findViewById(R.id.pendingList);
        list3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserProject = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.GroupDetails.class);
                startActivity(takeUserProject);
            }
        });



        ListView list4 = (ListView)findViewById(R.id.historyList);
        list4.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent takeUserProject = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.GroupDetailsDone.class);
                startActivity(takeUserProject);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_group_list, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(GroupList.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}
