package com.example.alexandra.cs464_mobile_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class LogIn_Activity extends Activity {

    /* Need to take these field from the View */
    protected EditText mLogin;
    protected EditText mPassword;
    protected Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_);

        /* Initialize the fields*/
        mLogin = (EditText)findViewById(R.id.usernameEditText);
        mPassword = (EditText)findViewById(R.id.passwordEditText);
        mLoginButton = (Button)findViewById(R.id.sign_in_button);

        //Listen when mLoginButton is clicked and then log in the user

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Get the user inputs and convert to string
                String login = mLogin.getText().toString().trim();
                String password = mPassword.getText().toString().trim();

                if ((login.equals("barka") && password.equals("asdf")) || (login.equals("degleri") && password.equals("1111"))
                        || (login.equals("user") && password.equals("user"))) {
                    //correct credentials
                    Toast.makeText(LogIn_Activity.this, "Welcome Back!", Toast.LENGTH_LONG).show();

                    //take user to home page
                    Intent takeUserHome = new Intent(LogIn_Activity.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                    startActivity(takeUserHome);

                }else{
                    //error, advice user
                    AlertDialog.Builder builder = new AlertDialog.Builder(LogIn_Activity.this);
                    builder.setMessage("Log In Failed. Invalid Log In Credentials.");
                    builder.setTitle("Sorry!");
                    builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //close the dialog
                            dialog.dismiss();
                        }
                    });
                    AlertDialog dialog = builder.create();
                    dialog.show();


                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_log_in_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }
}
