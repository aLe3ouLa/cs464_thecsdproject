package com.example.alexandra.cs464_mobile_app;

/**
 * Created by Alexandra on 26/04/2015.
 */
public class Project {

    private String projectName;
    private String courseName;
    private String numOfPeople;
    private int iconID;

    public Project(String projectName, String courseName, String numOfPeople, int iconID) {
        this.projectName = projectName;
        this.courseName = courseName;
        this.numOfPeople = numOfPeople;
        this.iconID = iconID;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(String numOfPeople) {
        this.numOfPeople = numOfPeople;
    }
}
