package com.example.alexandra.cs464_mobile_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;


public class ProfileCommentsMyView extends ActionBarActivity {

    private ImageButton mCalendar;
    private ImageButton mNotifications;
    private ImageButton mHome;
    private ImageButton mProfile;


    private Button details;

    private ImageButton mContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_comments_my_view);
        // Action Bar Buttons


        mCalendar = (ImageButton)findViewById(R.id.calendarBtn);
        mNotifications = (ImageButton)findViewById(R.id.notificationBtn);
        mHome = (ImageButton)findViewById(R.id.homeBtn);
        mProfile = (ImageButton)findViewById(R.id.manBtn);

        // Action Bar Buttons
        mHome.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //  Toast.makeText(ProfileComments.this, "Home", Toast.LENGTH_LONG).show();

                //take user to Home
                Intent takeUserHome = new Intent(ProfileCommentsMyView.this, com.example.alexandra.cs464_mobile_app.HomePage_Activity.class);
                startActivity(takeUserHome);
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //   Toast.makeText(ProfileComments.this, "Profile", Toast.LENGTH_LONG).show();

                //take user to Profile
                Intent takeUserHome = new Intent(ProfileCommentsMyView.this, com.example.alexandra.cs464_mobile_app.ProfileActivity.class);
                startActivity(takeUserHome);
            }
        });

        mCalendar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //   Toast.makeText(ProfileComments.this, "Search", Toast.LENGTH_LONG).show();

                //take user to Calendar
                Intent takeUserCal = new Intent(ProfileCommentsMyView.this, com.example.alexandra.cs464_mobile_app.CalendarDay.class);
                startActivity(takeUserCal);
            }
        });
        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Toast.makeText(ProfileComments.this, "Notifications", Toast.LENGTH_LONG).show();

                //take user to NotificationsListView
                Intent takeUserNotif = new Intent(ProfileCommentsMyView.this, NotificationsListView.class);
                startActivity(takeUserNotif);
            }
        });

        mNotifications.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent takeUserNotif = new Intent(ProfileCommentsMyView.this, ProfileComments.class);
                startActivity(takeUserNotif);
            }
        });

        details =(Button)findViewById(R.id.joinBtn);
        details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent takeUserNotif = new Intent(ProfileCommentsMyView.this, ProfileActivity.class);
                startActivity(takeUserNotif);
            }
        });



    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_profile_comments, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_search:
                openSearch();
                return true;
            case R.id.action_logout:
                openLogOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void openLogOut() {

        Intent takeUserCal = new Intent(ProfileCommentsMyView.this, com.example.alexandra.cs464_mobile_app.LogIn_Activity.class);
        startActivity(takeUserCal);
    }

    private void openSearch() {
        Intent takeUserCal = new Intent(ProfileCommentsMyView.this, com.example.alexandra.cs464_mobile_app.SearchEmpty.class);
        startActivity(takeUserCal);
    }
}