package com.example.alexandra.cs464_mobile_app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Eirini on 26-Apr-15.
 */
public class DataProvider {
    public static HashMap<String,List<String>> getInfo(){
        HashMap<String, List<String>> Search_Details = new HashMap<String,List<String>>();
        List<String> People = new ArrayList<String>();
        People.add("Giorgos Georgiou");

        List<String> Projects=new ArrayList<String>();
        Projects.add("User Interface Implementation");

        Search_Details.put("People",People);
        Search_Details.put("Projects",Projects);

        return Search_Details;
    }

}
