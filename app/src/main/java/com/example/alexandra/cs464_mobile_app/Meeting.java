package com.example.alexandra.cs464_mobile_app;


public class Meeting {

    private String courseName;
    private String day;
    private String time;
    private String place;

    private int icon1;
    private int icon2;
    private int icon3;

    private String person1;
    private String person2;
    private String person3;


    private int iconID;


    public Meeting(String courseName, String day, String time, String place, int icon1, int icon2, int icon3, String person1, String person2, String person3, int iconID) {
        this.courseName = courseName;
        this.day = day;
        this.time = time;
        this.place = place;
        this.icon1 = icon1;
        this.icon2 = icon2;
        this.icon3 = icon3;
        this.person1 = person1;
        this.person2 = person2;
        this.person3 = person3;
        this.iconID = iconID;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public int getIcon1() {
        return icon1;
    }

    public void setIcon1(int icon1) {
        this.icon1 = icon1;
    }

    public int getIcon2() {
        return icon2;
    }

    public void setIcon2(int icon2) {
        this.icon2 = icon2;
    }

    public int getIcon3() {
        return icon3;
    }

    public void setIcon3(int icon3) {
        this.icon3 = icon3;
    }

    public String getPerson1() {
        return person1;
    }

    public void setPerson1(String person1) {
        this.person1 = person1;
    }

    public String getPerson2() {
        return person2;
    }

    public void setPerson2(String person2) {
        this.person2 = person2;
    }

    public String getPerson3() {
        return person3;
    }

    public void setPerson3(String person3) {
        this.person3 = person3;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }
}